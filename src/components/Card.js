import React, { Component } from 'react';
import Konva from 'konva';
import { render } from 'react-dom';
import { Stage, Layer, Star, Text, Sprite } from 'react-konva';

class App extends Component {
  handleDragStart = e => {
    e.target.setAttrs({
      shadowOffset: {
        x: 15,
        y: 15
      },
      scaleX: 1.1,
      scaleY: 1.1
    });
  };
  handleDragEnd = e => {
    e.target.to({
      duration: 0.5,
      easing: Konva.Easings.ElasticEaseOut,
      scaleX: 1,
      scaleY: 1,
      shadowOffsetX: 5,
      shadowOffsetY: 5
    });
  };
  render() {    
    var theSprite1 = new Image();
    
    theSprite1.src =
      "/Users/humzaa/monkeyCode/reactOutclass/my-app/sprite-sheets/Front-Idle-Blinking.png";
    
    var animations = {
      standing: [0, 0, 80, 94],
      walking: [0, 94, 80, 94,
               80, 94, 80, 94,
               160, 94,  80, 94,
               240, 94, 80, 94,
               320, 94, 80, 94,
               400, 94, 80, 94],
      jumping: [0, 282, 80, 94,
               80, 282, 80, 94,
               160, 282, 80, 94]
    };
    
    theSprite1.onload = function() {
      var heroA = new Konva.Sprite({
        x: 50,
        y: 50,
        image: theSprite1,
        animation: 'standing',
        animations: animations,
        frameRate: 6,
        frameIndex: 0
      });
      
      var heroB = new Konva.Sprite({
        x: 50,
        y: 150,
        image: theSprite1,
        animation: 'walking',
        animations: animations,
        frameRate: 6,
        frameIndex: 0
      });
      
      var heroC = new Konva.Sprite({
        x: 50,
        y: 250,
        image: theSprite1,
        animation: 'walking',
        animations: animations,
        frameRate: 60,
        frameIndex: 0
      });
      
      var heroD = new Konva.Sprite({
        x: 275,
        y: 150,
        scale: 2,
        image: theSprite1,
        animation: 'jumping',
        animations: animations,
        frameRate: 2,
        frameIndex: 0
      });    
    }

    return (
      <Stage width={window.innerWidth} height={window.innerHeight}>
        <Layer>
          <Text text="Try to drag a star" />          
            <Sprite x={275} y={150} image={theSprite1}
            animation="jumping" animations={animations} frameRate={2} frameIndex={0}             
            />          
        </Layer>
      </Stage>
    );
  }
}

export default App;